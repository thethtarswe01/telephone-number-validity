#include <stdio.h>
#include <ctype.h>

#include "number.h"

// function to check if the correct number of arguments was provided
int check_arguments(int argc, char *argv[])
{
  if (argc < 2 || !argv || !argv[1])
  {
    return 0;
  }
  return 1;
}

// function to check if the phone number is valid
int check_number_validity(char *number)
{
  if (!number)
  {
    return 0;
  }

  int valid = 1;
  for (int i = 0; number[i] != '\0'; i++)
  {
    if (!isdigit(number[i]))
    {
      valid = 0;
      break;
    }
  }
  return valid;
}

// function to print the result
void print_result(int valid, char *number)
{
  if (!number)
  {
    return;
  }

  if (valid)
  {
    printf("The number is valid: %s\n", number);
  }
  else
  {
    printf("The number is not valid: usage: phone <number>\n");
  }
}


