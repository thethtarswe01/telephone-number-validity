#include <stdio.h>
#include <ctype.h>
#include <string.h>

// function to check if the correct number of arguments was provided
int check_arguments(int argc, char *argv[])
{
  if (argc < 2 || !argv || !argv[1])
  {
    return 0;
  }
  return 1;
}

// function to check if the phone number is valid
int check_number_validity(char *number)
{
  if (!number)
  {
    return 0;
  }

  int valid = 1;
  for (int i = 0; i < strlen(number); i++)
  {
    if (!isdigit(number[i]))
    {
      valid = 0;
      break;
    }
  }
  return valid;
}

// function to print the result
void print_result(int valid, char *number)
{
  if (!number)
  {
    return;
  }

  if (valid)
  {
    printf("The number is valid: %s\n", number);
  }
  else
  {
    printf("The number is not valid: usage: phone <number>\n");
  }
}

int main(int argc, char *argv[])
{
  // check if the arguments are valid
  int args_valid = check_arguments(argc, argv);
  if (!args_valid)
  {
    printf("Usage: phone <number>\n");
    return 1;
  }

  // check if the number is valid
  int valid = check_number_validity(argv[1]);

  // print the result
  print_result(valid, argv[1]);
}
