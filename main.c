#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
  // check if the arguments are valid
  int args_valid = check_arguments(argc, argv);
  if (!args_valid)
  {
    printf("Usage: phone <number>\n");

  }

  // check if the number is valid
  int valid = check_number_validity(argv[1]);

  // print the result
  print_result(valid, argv[1]);
}
