#ifndef NUMBER_H_INCLUDED
#define NUMBER_H_INCLUDED

int check_arguments(int argc, char *argv[]);
int check_number_validity(char *number);
void print_result(int valid, char *number);
int main(int argc, char *argv[]);


#endif // NUMBER_H_INCLUDED
